<?php
require '../vendor/autoload.php';

//Définition de constante pour le chemin des fichiers
define('WEBROOT', str_replace('index.php', '',$_SERVER['SCRIPT_NAME'])); //Affiche /monsite/src/
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME'])); /*Affiche C://WAMPSERVER/www/monsite/src/ */
define('URL', str_replace('src/index.php', '', $_SERVER['SCRIPT_NAME'])); //Affiche /monsite/

//Test
var_dump($_GET['p']);
var_dump(WEBROOT);
var_dump(ROOT);
var_dump(URL);



//Décomposition de l'url
if (isset($_GET['p']) && $_GET['p'] != '') {
$param = explode('/', $_GET['p']);
$controller = ucfirst(strtolower($param[0]));
$action = isset($param[1]) && $param[1] != "" ? $param[1] :
'index';
$id = isset($param[2]) && $param[2] != "" ? $param[2] : null;
} else {
$controller = 'Accueil';
$action = "index";
$id = null;
}
var_dump($controller);
var_dump($action);
var_dump($id);


    //On appelle notre controller si son fichier existe
if (file_exists(ROOT . 'controller/' . $controller . '.php')) {
    require_once ROOT . '/controller/' . $controller . '.php';
    $controllerVerifie = new $controller();
    //On appelle la méthode si elle est définit dans le fichier du controller
    if (method_exists($controllerVerifie, $action)) {
    $controllerVerifie->$action($id);
    }
    }

    //Panier ou connexion
if (!isset($_SESSION)) {
    session_start();
    }
    var_dump($_SESSION) ;
    
?>