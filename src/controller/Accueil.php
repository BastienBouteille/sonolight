<?php
class Accueil{

    private $vars=array();

    //La fonction 3. qui permet d'envoyer des variables à la vue
    function set($id)
    {
        $this->vars=array_merge($this->vars,$id);
    }

    function render($filename)
    {
        extract($this->vars);
        $chemin=ROOT."view/".strtolower(get_class($this))."/".
        $filename.".php";
        require($chemin);
    }
     
    public function index($id=null)
    {
        //Créer une variable
        $variable['accueil']=array("titre"=>"L'accueil du site",
        "description"=>"Le texte de l'accueil");
        
        //Envoyer la variable à la vue
        $this->set($variable);

        //Ici on appelle la vue présente dans le dossier view/accueil
        $this->render("index");
    }

    public function __construct()
    {
        echo 'Je suis l\'accueil';
    }


}
?>
 
 
<h1>accueil</h1>
<div>
partie principale de l'accueil
</div>