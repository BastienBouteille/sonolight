<?php
class  Photo
{
    private $vars=array();

   public  function render($filename)
{
    extract($this->vars);
    $chemin=ROOT."view/".strtolower(get_class($this))."/".
    $filename.".php";
    require($chemin);
}

public function index($id=null)
{
    $variable['Photo']=array("titre"=>"La page de publication",
    "description"=>"Le texte de la publication");

    $this->set($variable);
    $this->render("index");
    
    $variable['autrePhoto']=array("titre"=>"L'autre page de publication",
    "description"=>"L'autre texte de la publication");

    $this->set($variable);
    $this->render("autrePhoto");
}

public function Photo($id=null)
{
    $variable['autrePhoto']=array("titre"=>"L'autre page de publication",
    "description"=>"L'autre texte de la publication");

    $this->set($variable);
    $this->render("autrePhoto");
}


public function __construct()
{
    echo 'je suis la page de publication';
}



public function set($id){
    $this->vars=array_merge($this->vars,$id);
}

}
?>